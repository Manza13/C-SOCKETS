--> Español:

  <<--Comentarios, en Español -->>

Programa desarrollado en el lenguaje de programacion C, es un programa
Cliente/Servidor, en el cual puedes mandar mensajes al servidor, y lo recibe.

Este programa usa "SOCKETS", y cada linea se encuentra explicada.

--> English:

  <<-- Comments, in Spanish -->>

Program developed in the programming language C, is a program
Client / Server, in which you can send messages to the server, and receive it.

This program uses "SOCKETS", and each line is explained.
